﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kata1Colecciones
{
    public class Municipio
    {
        public string nombre;
        public string departamento;
        public List<string> ciudadesAledañas = new List<string>();
        
    }
}
