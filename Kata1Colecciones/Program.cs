﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kata1Colecciones
{
    class Program
    {
        static void Main(string[] args)
        {
            gestor gestionarMunicipios = new gestor();

            //Metodo para agregar nuevo objeto
            gestionarMunicipios.adicionarNuevoObjeto("Medellín", "Antioquia", "Bello");
            gestionarMunicipios.adicionarNuevoObjeto("Bogota", "Cundinamarca", "Soacha");
            gestionarMunicipios.adicionarNuevoObjeto("Cartagena", "Bolivar", "Turbana");

            //Metodo para agregar una ciudad aledaña a un objeto creado, recibe como parametro
            //la posición de en la lista de la ciudad, y la ciudad aledaña que se agrega
            gestionarMunicipios.adicionarCiudadAledaña(0, "Itagui");
            gestionarMunicipios.adicionarCiudadAledaña(1, "Bogota");

            //Metodo para listar los objetos de la colección
            gestionarMunicipios.listarObjetos();

            //Metodo para eliminar una ciudad aledaña, recibe como parametro
            //la posicón de la ciudad y el nombre de la ciudad aledaña a eliminar
            gestionarMunicipios.eliminarCiudadAledaña(0, "Bello");

            //Metodo para eliminar una ciudad, recibe como parametro la posición de la ciudad
            //a eliminar
            gestionarMunicipios.eliminarLaCiudad(1);

            //Metodo para editar una ciudad de la colección, recibe como parametro
            //La ubicación en la lista, y los nuevos datos
            gestionarMunicipios.editarCiudad(0, "La estrella", "Antioquia", "Medellín");

            //Metodo que filtra un municipio por el nombre.
            gestionarMunicipios.filtrarPorNombre("Medellín");


            //Metodo para filtrar por el nombre de la ciudad aledaña.
            gestionarMunicipios.filtrarPorCiudadAledaña("Bello");

            //Filtra por el nombre del departamento
            gestionarMunicipios.filtrarPorDepartamento("Bolivar");

            Console.ReadKey();
        }
    }
}
