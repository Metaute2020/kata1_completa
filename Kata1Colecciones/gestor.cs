﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kata1Colecciones
{
    public class gestor
    {
        List<Municipio> ListMunicipios = new List<Municipio>();
        public void adicionarNuevoObjeto(string Nombre, string Departamento, string CiudadAledaña)
        {
            Municipio municipio = new Municipio();
            municipio.nombre = Nombre;
            municipio.departamento = Departamento;
            municipio.ciudadesAledañas.Add(CiudadAledaña);
            ListMunicipios.Add(municipio);


        }

        public void adicionarCiudadAledaña(int Posicion, string NewCiudadAledaña)
        {
            ListMunicipios[Posicion].ciudadesAledañas.Add(NewCiudadAledaña);

        }

        public void listarObjetos()
        {
            Console.WriteLine("********Lista de municipios.********");
            for (int i = 0; i < ListMunicipios.Count; i++)
            {
                Console.WriteLine("Municipio: " + ListMunicipios[i].nombre);
                Console.WriteLine("Departamento: " + ListMunicipios[i].departamento);
                Console.WriteLine("Ciudades Aledañas: ");
                foreach (var ciudad in ListMunicipios[i].ciudadesAledañas)
                {
                    Console.Write(ciudad + " \n");
                }
                Console.WriteLine("****************");
            }
        }

        public void eliminarCiudadAledaña(int Posicion, string ciudadAledaña)
        {
            ListMunicipios[Posicion].ciudadesAledañas.Remove(ciudadAledaña);
        }

        public void eliminarLaCiudad(int Posicion)
        {
            ListMunicipios.RemoveAt(Posicion);
        }

        public void editarCiudad(int Posicion, string Nombre, string Departamento, string CiudadAledaña)
        {
            ListMunicipios.RemoveAt(Posicion);
            Municipio municipio = new Municipio();
            municipio.nombre = Nombre;
            municipio.departamento = Departamento;
            municipio.ciudadesAledañas.Add(CiudadAledaña);
            ListMunicipios.Insert(Posicion, municipio );

                        Console.WriteLine("********Lista de municipios.********");
            for (int i = 0; i < ListMunicipios.Count; i++)
            {
                Console.WriteLine("Municipio: " + ListMunicipios[i].nombre);
                Console.WriteLine("Departamento: " + ListMunicipios[i].departamento);
                Console.WriteLine("Ciudades Aledañas: ");
                foreach (var ciudad in ListMunicipios[i].ciudadesAledañas)
                {
                    Console.Write(ciudad + " \n");
                }   
            }
        }

        public void filtrarPorNombre(string nombre)
        {
            int contador = 0;
            foreach (var municipio in ListMunicipios)
            {
                int numero = municipio.nombre.IndexOf(nombre);
                
                ++contador;
                
                if (numero >= 0)
                {
                    Console.WriteLine("Filtro por Ciudad");
                    Console.WriteLine("Nombre ciudad: " + ListMunicipios[contador-1].nombre);
                    Console.WriteLine("Departamento: " + ListMunicipios[contador-1].departamento);
                    foreach (var ciudad in ListMunicipios[contador-1].ciudadesAledañas)
                    {
                        Console.Write("Ciudad aledaña: "+ciudad + " \n");
                    }

                }
            }
        }

        public void filtrarPorCiudadAledaña(string nombre)
        {
            int contador = 0;
            foreach (var municipio in ListMunicipios)
            {
                int index = municipio.ciudadesAledañas.IndexOf(nombre);
                contador++;
                if (index >= 0)
                {
                    Console.WriteLine("Filtro por Ciudad aledaña");
                    Console.WriteLine("Nombre ciudad: " + ListMunicipios[contador - 1].nombre);
                    Console.WriteLine("Departamento: " + ListMunicipios[contador - 1].departamento);
                    foreach (var ciudad in ListMunicipios[contador - 1].ciudadesAledañas)
                    {
                        Console.Write("Ciudad aledaña: " + ciudad + " \n");
                    }

                }
            }

        }

        public void filtrarPorDepartamento(string departamento)
        {
            int contador = 0;
            foreach (var municipio in ListMunicipios)
            {
                int index = municipio.departamento.IndexOf(departamento);
                contador++;
                if (index >= 0)
                {
                    Console.WriteLine("Filtro por departamento");
                    Console.WriteLine("Nombre ciudad: " + ListMunicipios[contador - 1].nombre);
                    Console.WriteLine("Departamento: " + ListMunicipios[contador - 1].departamento);
                    foreach (var ciudad in ListMunicipios[contador - 1].ciudadesAledañas)
                    {
                        Console.Write("Ciudad aledaña: " + ciudad + " \n");
                    }

                }
            }

        }
    }
}
